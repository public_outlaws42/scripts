#!/bin/sh

# Directory to copy from on the source machine. Ending slash copies contents of dir only
SOURCEFILE="/home/troy/Multimedia/Videos/10-04-2009.avi"

# Directory to copy to on the destination machine.
DESTFILE="/home/troy/Multimedia/Videos/flv/10-04-2009.flv "



# Options.
# -r Set Frames Per Second (fps) Example:25
# -ar freq Set the audio sampling frequency (default = 44100 Hz).
# -s Set frame size. The format is wxh (ffserver default = 160x128, ffmpeg default = same as source).
# -qscale Use fixed video quantizer scale (1=best quality. Higher the number worst quality)
# See man ffmpeg for other options.

# For fcogmedia sermon video.
#OPTS="-r 25 -ar 44100 -s 320x240 -qscale 7 "
# fcogmedia Community Update
OPTS="-r 25 -ar 44100 -s 320x240 -qscale 5 "





 ffmpeg -i $SOURCEFILE $OPTS $DESTFILE

