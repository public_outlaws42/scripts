#!/bin/bash

# backup.sh
# Makes a backup of files from a source directory to a target directory.

# Checks for the number of arguments entered $# represents the count at the command
if [ $# -ne 2 ]
then
  echo "Usage: backup.sh <source_directory> <target_directory>"
  echo "Please try again"
  exit 1
fi

# Check to see if rsync is installed on your computer
if ! command -v rsync > /dev/null 2>&1
then
  echo "This script requires rsync." 
  echo "please install with the package manager for your Linux distro"
  exit 2
fi

# Get the current date
current_date=$(date +%Y-%m-%d)

# rsync options
rsync_options="-avb --backup-dir $2/$current_date"

# rsync command to run for backup
$(which rsync) $rsync_options $1 $2/current >> Logs/backup_$current_date.log

