#!/usr/bin/bash

function findEXT(){

  ext="$1"
  current_dir=$(echo $PWD)
  word=$( /usr/bin/zenity --entry --entry-text="emily" --title="$ext - Search word" --text="Enter the word you want to search.\n This will wearch $ext files")
  echo $word
  result_header="Results for 'Open Document Text' files"
  if [[ -z $word ]]; then
     /usr/bin/zenity --warning --width=250 --title="NO KEYWORD" --text="Text field can't be empty!"

  else
    results=$(find $current_dir -type f -iname "*$word*.$ext" -printf '"%p"\n')
    if [[ -z $results ]]; then
      /usr/bin/zenity --info --width=250 --title="$result_header" --text="No results found for '$word' and extension '$ext'"
    else
      echo ${results[@]}
      select=$(eval /usr/bin/zenity --list --width=700 --height=500 --column="Results" $results) 
    fi

  fi
  if [[ -z $select ]]; then
    return 1
  else
    nemo "$select" 
  fi
}


