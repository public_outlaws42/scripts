#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Backup database dumps from a remote server. getting server
from a config file.
"""
# Imports
from pathlib import Path
import subprocess


__author__ = "Troy Franks"
__version__ = "2023-12-10"

config = ".config/racetrack.conf"


def home_dir():
    """
    returns the users home dir
    requires the pathlib module
    """
    return Path.home()


def open_file(
    fname: str,
) -> list:
    """
    fname = filename,
    takes config file and reads all lines and puts
    it into a list.
    """
    home = home_dir()
    content = []
    try:
        with open(f"{home}/{fname}", "r") as path_text:
            for line in path_text:
                content.append(line.rstrip())
    except FileNotFoundError as e:
        print(e)
        print("It is reading here")
        with open(f"{home}/{fname}", "w") as output:
            output.write("# IP Address")
    return content


def write_file(
    fname: str,
    var,
):
    """
    fname = filename,
    writes the log file.
    """
    home = home_dir()
    try:
        with open(f"{home}/{fname}", "w") as output:
            output.write(var)
    except FileNotFoundError as e:
        print(e)


config_file: list = open_file(
    fname=config,
)

ip: str = config_file[1]
user: str = config_file[4]
log_file: str = config_file[7]
source_dir: str = config_file[10]
dest_dir: str = config_file[13]

command = subprocess.run(
    [
        "/usr/bin/rsync",
        "-auvh",
        "--progress",
        "--stats",
        f"{user}@{ip}:{source_dir}",
        f"{dest_dir}",
    ],
    stdout=subprocess.PIPE,
    stderr=subprocess.PIPE,
    text=True,
)

# write log file
log = command.stdout + command.stderr

write_file(
    fname=log_file,
    var=log,
)
