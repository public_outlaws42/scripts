#!/bin/sh

# excludes file - Contains wildcard patterns of files to exclude.
# i.e., *~, *.bak, etc.  One "pattern" per line.
# You must create this file.
EXCLUDES="${HOME}/bin/Scripts/Backup/exclude.txt"

INCLUDES="${HOME}/bin/Scripts/Backup/include.txt"

# Options Descriptions.
# -n Don't do any copying, but display what rsync *would* copy. For testing.
# -a Archive. Mainly propogate file permissions, ownership, timestamp, etc.
# -u Update. Don't copy file if file on destination is newer.
# -v Verbose -vv More verbose. -vvv Even more verbose.
# See man rsync for other options.

# Options
OPTS="-auvh --progress --stats --out-format='%f' --exclude-from=$EXCLUDES" # (4)

# Directory to be backed up.
BACKDIR="$HOME" 

# Directory to copy too.
DESTDIR="pi@backup:/mnt/backupt/Backup/race42/" 

rsync $OPTS $BACKDIR $DESTDIR | tee $HOME/Logs/net_backup.log
