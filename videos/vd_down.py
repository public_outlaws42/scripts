#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Download videos from youtube using yt-dlp. playlists are put into channel/playlist title.
requires yt-dlp to be installed. All other dependencies are std python libraries 

"""
# Imports
from subprocess import run
from pathlib import Path

__author__ = "Troy Franks"
__version__ = "2024-07-09"


def open_txt_readlines(
    fname: str,
) -> list:
    """
    Returns a list from a txt file divided by lines.
    this will remove the newline character and empty lines.
    It will skip lines that start with a #
    """
    home = Path.home()
    output = []
    try:
        with open(f"{home}/{fname}", "r", encoding="utf-8") as file:
            for line in file:
                if not line.startswith("#"):
                    if line.strip():
                        output.append(line.strip())
    except (FileNotFoundError, EOFError) as e:
        print(e)

    return output

PLAYLIST_FORMAT: str = "%(channel)s/%(playlist_title)s/"
FILE_FORMAT: str = "%(title)s-%(upload_date)s-%(vcodec)s-%(acodec)s-%(autonumber)02d.%(ext)s"
BATCH_FILE: str = ".dotfiles/.config/yt-dlp/dlp.txt"

def main() -> None:
    """
    Get the url's and run the video download command
    """
    file_open = open_txt_readlines(BATCH_FILE)
    if len(file_open) > 0:
        for i in file_open:
            if "playlist" in i:
                run([
                    "yt-dlp",
                    "--no-part", 
                    "--yes-playlist",
                    f"-o {PLAYLIST_FORMAT}%(playlist_index)s - {FILE_FORMAT}"
                ],
                            check=False,
                               )
            else:
                run(["yt-dlp","--no-part"],check=False)
    else:
        print("Nothing to download")

if __name__ == "__main__":
    main()
