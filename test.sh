#!/bin/bash

fish_config=$HOME/.config/fish/config.fish
if ! cat $HOME/.bashrc | grep -iw exa > /dev/null 2>&1
then
  echo "alias ls='exa -l --color=always --group-directories-first'" >> $HOME/.bashrc
fi

if ! cat $HOME/.config/fish/config.fish | grep -iw "exa" ;then
  echo "abbr ls \"exa -l --group-directories-first\"" >> $HOME/.config/fish/config.fish
fi

if [ -f $fish_config ] && ! cat $fish_config | grep -iw "exa"; then
 echo "abbr ls \"exa -l --group-directories-first\"" >> $fish_config 
fi

if [ -f $fish_config ]; then
  echo "fish config exists"
fi

test_dir=$HOME/test_me.txt

# Test if file exists
if [ -f test_dir  ]; then
  echo "Im here"
else
  echo "Im not here"
fi

