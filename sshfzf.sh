#!/bin/bash
###################################################################### 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###################################################################### 

function main(){
  cmd="$(grep '^function' "$0"|grep -v "function main"|awk '{print $2}'|cut -d\( -f1|fzf --border --header "SSH Menu" --header-lines=1 --prompt "SSH Menu Please Make a Selection: ")"
  $cmd
  exit 0
}

function ExitMenu(){
  echo "Good Bye..."
  cmd="exit"
  $cmd
}

function Cara(){
  cmd="ssh cara@eldora"
  $cmd
}

function HomeBackup(){
  # Backup Pi
  cmd="ssh pi@home"
  $cmd
}

function MythtvBackend(){
  # Mythtv
  cmd="ssh troy@mythtvbackend"
  $cmd
}

function RaceTrack(){
  # Weather Server
  cmd="ssh troy@racetrack"
  $cmd
}

function Sprintcars(){
  # 3D printer
  cmd="ssh troy@sprintcars"
  $cmd
}

main

