#!/bin/bash
i3-msg workspace 1 2>&1 1>/dev/null
kitty & 
sleep 2
i3-msg split horizontal 2>&1 1>/dev/null
sleep 2
kitty &
sleep 2
i3-msg split vertical 2>&1 1>/dev/null
sleep 2
kitty &
sleep 2 
i3-msg focus left; i3-msg split vertical 2>&1 1>/dev/null
sleep 2
kitty &

#sleep 0.2
#i3-msg layout splitv
#kitty  &
#urxvt -hold -cd ${PWD} &
#sleep 0.2
#mupdf main.pdf &
#sleep 0.2
#i3-msg 'focus left, focus left, split v'
#sleep 0.2
#urxvt -hold -e zsh -c "vim ${PWD}/main.tex" &
#sleep 0.2
#i3-msg 'focus right, focus right, split v'
#sleep 0.2
#urxvt -hold -cd ${PWD} &
#sleep 0.2
#i3-msg 'resize shrink height 30 px or 30 ppt'
#sleep 0.2
#i3-msg 'focus left'
#sleep 0.2
#vim ${PWD}/style.sty