#!/usr/bin/env bash
set +x
# ====================================
# Author: Troy Franks
# Purpose: Resize images selected in 
# the current folder. To be used with 
# Nemo Scripts feature
# Location: ~/.local/share/nemo/scripts
# =====================================
version="1.0.6"

# ========Major Variables==================== 
# ===========================================

# file path when from nemo and all selected files
files=($@)
isPath=$(basename "${files[0]}")

# The output dir name this will be created
# if it doesn't exist it will be created
output_dir=output

# Whether to use a prefix 0=no, 1=yes
prefix=0

# ========Functions==================
# ===================================
function cancel(){
  if [[ -z $size ]]; then
    exit 1
  fi
}

function source() {
  fileNames=()
  if [[ -f $isPath ]]; then
    current_dir=$(echo $PWD)
    for i in "${files[@]}"; do
      fileNames+=($i)
    done
  else
    current_dir=${files[0]}
    cd $current_dir
    unset files[0] # Delete array item
    # Loop through an array of paths and get the filename
    # and append it to another array.
    for i in "${files[@]}"; do
      temp=$(basename "$i")
      fileNames+=($temp)
    done

  fi
}

function sizeFunc(){
  size=$( /usr/bin/zenity --entry \
    --entry-text=1000 --title="Size" \
    --text="Enter the size you want the image on the biggest side")


}
# ==========Commands==================
# ====================================

source
sizeFunc
cancel


(

# =========zenity Progress=============

# ------------------------------------
echo "# Checking output folder..." 

if [[ ! -d $(echo "${current_dir}/${output_dir}") ]];then
  mkdir "${current_dir}/${output_dir}"
fi 

# -----------------------------------
echo "25"
echo "# Converting..." ; sleep 1
if [ $prefix -eq 0 ]; then
  for i in "${fileNames[@]}"; do /usr/bin/mogrify -path "${output_dir}" \
    -quality 90 \
    -resize ${size}x${size} \
    $i; done
else
  for i in "${fileNames[@]}"; do /usr/bin/convert \
    -quality 85 \
    -resize ${size}x${size} \
    $i re_$i; done
fi

# ----------------------------------
echo "50"
echo "# Moving files..." ; sleep 1
if [ $prefix -ne 0 ]; then
  for f in re_*.*; do mv $f $current_dir/$output_dir; done
fi

# ----------------------------------
echo "75"
echo "# Cleaning Up..." ; sleep 1

# ----------------------------------
echo "99"
echo "# Finishing..." ; sleep 1


# ----------------------------------
echo "# All finished." ; sleep 1
echo "100"

) |
/usr/bin/zenity --progress \
  --title="Converting Images" \
  --text="Running..." \
  --percentage=10 \
  --auto-close

# ========Zenity Info=========================
/usr/bin/zenity --info \
  --text="${#fileNames[@]} resized images will be stored in ${current_dir}/${output_dir}"
