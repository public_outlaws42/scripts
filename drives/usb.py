#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Gets the device id of external drive(s) on a linux system and mounts them.
lsblk is require to be installed on the system. It is probably on most.
"""
# Imports
import tomllib
from subprocess import run, PIPE
from pathlib import Path
import typer
from typing_extensions import Annotated
from rich import print

app = typer.Typer()

__author__ = "Troy Franks"
__version__ = "2024-10-25"


def setup() -> dict:
    """
    Returns the config options
    """
    home = Path.home()
    with open(f"{home}/.config/usb/config.toml", encoding="utf-8") as conf:
        content = conf.read()
        config = tomllib.loads(content)
    return config


DRIVES: str = "ls -l /dev/disk/by-id/*usb* | awk '/sd../ {print $NF}'"
CONFIG = setup()
HELP_ACTION: str = CONFIG["Help"]["specify_action"]
HELP_DRIVE: str = CONFIG["Help"]["specify_drive"]


@app.command()
def ls() -> None:
    """
    lists the connected USB drives.
    """
    drives: list = query_drives(DRIVES)
    mounted: dict = check_drives(drives)

    output(drives, mounted)


@app.command()
def mount() -> None:
    """
    Gets the USB drives that can be mounted and then mounts them.
    """
    drives: list = query_drives(DRIVES)
    mounted: dict = check_drives(drives)
    output(drives, mounted)
    mnt_umnt(mounted, "mount")


@app.command()
def unmount() -> None:
    """
    Gets the USB drives that are mounted and then unmounts them.
    """
    drives: list = query_drives(DRIVES)
    mounted: dict = check_drives(drives)
    output(drives, mounted)
    mnt_umnt(mounted, "unmount")


@app.command()
def specify(
    drive: Annotated[
        str,
        typer.Option(help=HELP_DRIVE),
    ],
    action: Annotated[
        str,
        typer.Option(
            help=HELP_ACTION,
        ),
    ] = "mount",
) -> None:
    """
    Mounts or unmounts the specified USB drive depending on what is specified
    on the commandline. The options are 'mount' or 'unmount'
    example: usb.py specify --action mount --drive sdd1
    """
    drives: list = query_drives(DRIVES)
    if len(drives) >= 1:
        mnt_umnt(drives=drive, action=action)
    else:
        print("No detected drives found")


def output(
    drives: list,
    mounted: dict,
):
    """
    output status of drives.
    """
    status: str = ""
    if len(drives) >= 1:
        for i in drives:
            if mounted[i] is True:
                status = "mounted"
            else:
                status = "unmounted"

            print(f"/dev/{i} is {status}")
    else:
        print("No detected drives found")


def check_drives(drive) -> dict:
    """
    checks to see the drive(s) mount status.
    """
    drive_mounted = {}
    for d in drive:
        drive_mounted[d] = check_mounted(f"lsblk -o MOUNTPOINT /dev/{d}").strip()
        if drive_mounted[d]:
            drive_mounted[d] = True
        else:
            drive_mounted[d] = False
    return drive_mounted


def mnt_umnt(
    drives,
    action: str = "mount",
) -> None:
    """
    mounts/unmounts drives in the dict.
    """
    what_type = type(drives)

    if what_type is dict:
        for k, v in drives.items():
            if action == "mount" and v is False or action == "unmount" and v is True:
                mount_action(drive=k, action=action)
    elif what_type is str:
        if action in ("mount", "unmount"):
            mount_action(drive=drives, action=action)
    else:
        print(
            "The drive needs to be either a dictionary of drives or a single drive as a string"
        )


def mount_action(drive: str, action: str) -> None:
    """
    Mounts or unmounts the specified drive depending on the action specified.
    """
    run(
        f"udisksctl {action} -b /dev/{drive}",
        shell=True,
        check=False,
    )


def check_mounted(url) -> str:
    """
    Checks to see if there are any external usb drives and
    returns a string of the drive device if any exist.
    """
    drive = (
        run(
            url,
            shell=True,
            stdout=PIPE,
            check=False,
        )
        .stdout.decode("utf-8")
        .replace("\n", " ")
        .lstrip("MOUNTPOINT")
    )
    return drive


def query_drives(url) -> list:
    """
    Checks to see if there are any external usb drives and
    returns a list of the drive(s) if any exist.
    """
    drives = (
        run(
            url,
            shell=True,
            stdout=PIPE,
            stderr=PIPE,
            check=False,
        )
        .stdout.decode("utf-8")
        .splitlines()
    )
    drives = [
        i[6:] for i in drives
    ]  # remove extra 2 characters at the beggining of each item.
    return drives


if __name__ == "__main__":
    app()
