# usb

This was intended to be a simple script to easily mount all external USB drives that are plugged in.

## Working on

I am fixing bugs as well as typos. Spelling and grammer is NOT my strong suit. :)

---

## Prerequisites

- `Python 3.11` at a minimum. I utilize the built in toml library that comes in 3.11 and later.
- `typer` needs to be installed.
- `rich` - needs to be installed.

---

## Installing

### Install Python

- Note any modern Linux system will already have python 3 installed.

**Debian & Ubuntu**

```bash
sudo apt-get install python3

```

**Arch**

```bash
sudo pacman -S python
```

### Install pip

`pip` is a package manager for `python` we will use it to install requirements for `usb`. Sometimes it isn't installed itself.You can install like this.

**Debian Ubuntu**

```bash

 sudo apt install python3-pip
```

**Arch**

```bash
sudo pacman -S python-pip
```

### Install dependencies with pip

```bash
pip3 install -r requirements.txt

```

You will have to make the python files executable.

```bash
chmod u+x usb.py

```

---

## To run

**usb.py command** This is the main file and is what you would run.

To run the script from the current directory.

```bash
./usb.py command

```

To see what commands are avaliable you can enter `usb.py --help`

## License

GPL 2.0
