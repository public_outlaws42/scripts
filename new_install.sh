#!/bin/bash
set +x
# ========================================================
# Author: Troy Franks
# This script is for installing software I use often that
# is not normally installed by default.
#
#
# ========================================================
version="1.0.19"


reset=$(tput sgr0)
bold=$(tput bold)
black=$(tput setaf 0)
red=$(tput setaf 1)
green=$(tput setaf 2)
yellow=$(tput setaf 3)
blue=$(tput setaf 4)
magenta=$(tput setaf 5)
cyan=$(tput setaf 6)
white=$(tput setaf 7)


# Color variables
#red='\033[0;31m'
#green='\033[0;32m'
#yellow='\033[0;33m'
#blue='\033[0;34m'
#magenta='\033[0;35m'
#cyan='\033[0;36m'
#bold= '\033[1m'
# Clear the color after that
#clear='\033[0m'

function distro(){
  if [ -f /etc/os-release ]
  then
    # freedesktop.org and systemd
    . /etc/os-release
    os=$NAME
    os_ver=$VERSION_ID
  elif command lsb_release > /dev/null 2>&1
  then
    os=$(lsb_release -si)
    os_ver=$(lsb_release -sr)
  elif [ -f /etc/lsb-release ]
  then
    os=$DISTRIB_ID
    os_ver=$DISTRIB_RELEASE
  else
    # Fall back to older versions of linux and BSD
    os=$(uname -s)
    os_ver=$(uname -r)
 fi
}

function os(){
  if [[ $OSTYPE =~ ^darwin ]]
  then
    os=$(sw_vers -productName)
    os_ver=$(sw_vers -productVersion)
  else
    distro

  fi
  echo -e "${bold}${blue}$os ${green}$os_ver${reset}"
}
function baseCheck(){
  if [ -f /etc/upstream-release/lsb-release ]
  then
    . /etc/upstream-release/lsb-release
    base_Id=$DISTRIB_ID
    base_release=$DISTRIB_RELEASE
    base=$(cat /etc/upstream-release/lsb-release | grep -i release | tr --delete DISTRIB_RELEASE= | tr --delete .)
  elif [[ $os = "Ubuntu" ]]
  then
    base_Id=$os
    base_release=$os_ver
    base=$(cat /etc/*-release | grep -i VERSION_ID | tr -d VERSION_ID= | tr -d . | tr -d \")
    base=$(($base+0))

  else
    base_Id=$os
    base_release=$os_ver
    base=$os_ver
  fi

}

function pm(){
  if command -v "apt-get" > /dev/null 2>&1 ; then 
    manager="apt-get install" # All debian based distro's
  elif command -v "dnf" > /dev/null 2>&1 ; then manager="dnf install" # Fedora
  elif command -v "yum" > /dev/null 2>&1 ; then manager="yum install" # Redhat
  elif command -v "zypper" > /dev/null 2>&1 ; then manager="zypper install" # Opensuse
  elif command -v "pacman" > /dev/null 2>&1 ; then manager="pacman -S" # Arch
  fi
}

function progExist(){
  program=$1
  if which $program > /dev/null 2>&1 ; then
    echo -e "${bold}${yellow}$program${reset} is ${bold}${green}already installed${reset}"
    return 1
  else
     echo -e "${bold}${yellow}$program${reset} is ${bold}${red}NOT${bold}${blue} installed${reset}"
    return 0
  fi
}

function progRec(){

  if ! which $1 > /dev/null 2>&1 ; then
    echo -e "${bold}${yellow}$1${reset} is ${bold}${red} required to be installed"
    echo -e "Installing now ${reset}"
    sudo $manager $1
  fi
}

function dirExists(){
  if [[ -e $1 || -f $1 ]] > /dev/null 2>&1; then
    echo -e "${bold}${yellow}$1${bold}${green} already exists${reset}"
    echo -e "${bold}${cyan}This would indicate that $2 is already installed${reset}"
    return 1
  else
    echo -e "${bold}${yellow}$2${reset} is ${bold}${red}NOT${bold}${blue} installed${reset}"
    return 0
  fi 

}

function checkX(){
  gui=$(type Xorg 2>/dev/null)
  if [[ $gui != Xorg* ]];then
    echo -e "${bold}${red}No graphical Environment found${reset}"
    return 1
  else
    echo -e "${bold}${green}Graphical environment found${reset}"
    echo -e "${bold}${blue}$XDG_CURRENT_DESKTOP${reset}"
    return 0
  fi
}

function installing(){
  program=$1
  echo -e "${bold}${green}Installing $program ... ${reset}"

}

function postInstall(){
  program=$1
  log_file=$2
  clear
  echo -e "${bold}${green}$program is finished installing${reset}"
  echo -e "${bold}${green}Check $log_file for more details${reset}"

}

function fishShell(){
  pm
  baseCheck
  program="fish"
  log_file="fish.log"
  exists=$(progExist $program > /dev/null 2>&1; echo $?)
  if [ $exists -eq 1 ]; then
    progExist $program
  elif [[ $base_Id = "Ubuntu" && $base -le 1804 ]]; then
    #installing $program
    sudo apt-add-repository ppa:fish-shell/release-3 >> $log_file 2>&1
    sudo apt-get update >> $log_file 2>&1
    sudo apt-get install fish >> $log_file 2>&1
  else
    #installing $program
    sudo $manager $program
    #postInstall $program $log_file
  fi
  # if fish doesn't exist create it in .bashrc
  if ! cat $HOME/.bashrc | grep -iw fish > /dev/null 2>&1
  then
    echo "fish" >> $HOME/.bashrc
    
  fi
}

function exa(){
  pm
  baseCheck
  program="exa"
  fish_config=$HOME/.config/fish/config.fish
  exists=$(progExist $program > /dev/null 2>&1; echo $?)
  if [ $exists -eq 1 ]; then
    progExist $program
  elif [[ $base_Id = "Ubuntu" && $base -lt 2010 ]]; then
    wget -c https://github.com/ogham/exa/releases/download/v0.10.0/exa-linux-x86_64-v0.10.0.zip
    unzip exa-linux-x86_64-v0.10.0.zip
    cd bin
    sudo mv exa /usr/local/bin
  else
    sudo $manager exa
  fi
  # if the alias for exa doesn't exist create it in .bashrc and in fish
  if ! cat $HOME/.bashrc | grep -iw exa > /dev/null 2>&1
  then
    echo "alias ls='exa -l --group-directories-first --header'" >> $HOME/.bashrc
  fi
  if [ -f $fish_config ] && ! cat $fish_config | grep -iw "exa" > /dev/null 2>&1 ; then
    echo "abbr ls \"exa -l --group-directories-first --header\"" >> $fish_config 
  fi
}

function kitty(){
  gui_exists=$(checkX  > /dev/null 2>&1; echo $?)
  if [ $gui_exists -eq 1 ];then
    checkX
  else
    echo "Ready to install"
    return 1
    curl -L https://sw.kovidgoyal.net/kitty/installer.sh | sh /dev/stdin
    echo "kitty is up to date"
  fi
}

function nala(){
  baseCheck
  program="nala"
  fish_config=$HOME/.config/fish/config.fish
  exists=$(progExist $program > /dev/null 2>&1; echo $?)
  if [ $exists -eq 1 ]; then
    progExist $program
  elif [[ $base_Id = "Ubuntu" && $base -le 1804 ]] ; then
    echo "Your OS version is to old to install nala"
    echo "Consider installing from source"
  elif [[ $base_Id = "Ubuntu" && $base -le 2104 ]]; then

    echo "deb [arch=amd64,arm64,armhf] http://deb.volian.org/volian/ scar main" | sudo tee /etc/apt/sources.list.d/volian-archive-scar-unstable.list
    wget -qO - https://deb.volian.org/volian/scar.key | sudo tee /etc/apt/trusted.gpg.d/volian-archive-scar-unstable.gpg > /dev/null
    sudo apt update && sudo apt install nala-legacy
  else
    sudo apt update && sudo apt install nala
  fi
  # if the alias for exa doesn't exist create it in .bashrc and in fish
  if ! cat $HOME/.bashrc | grep -iw nala > /dev/null 2>&1
  then
    echo "#Install with nala\nalias install='sudo nala install'\nalias remove='sudo nala remove'\nalias autoremove='sudo nala autoremove'\nalias clean='sudo nala clean'\nalias sr='sudo nala search'\n\n" >> $HOME/.bashrc

    echo "#Update with nala\nalias update='sudo nala update'\nalias upgradable='sudo nala list --upgradable'\nalias upgrade='sudo nala upgrade'\n\n" >> $HOME/.bashrc
  fi
  if [ -f $fish_config ] && ! cat $fish_config | grep -iw "nala" > /dev/null 2>&1 ; then
    echo "#Install with nala\nabbr install 'sudo nala install'\nabbr remove 'sudo nala remove'\nabbr autoremove 'sudo nala autoremove'\nabbr clean 'sudo nala clean'\nabbr sr 'sudo nala search'\n\n" >> $fish_config

    echo "#Update with nala\nabbr update 'sudo nala update'\nabbr upgradable 'sudo nala list --upgradable'\nabbr upgrade 'sudo nala upgrade'\n\n" >> $fish_config
 
  fi

}

function syncthing(){
  pm
  program="syncthing"
  exists=$(progExist $program > /dev/null 2>&1; echo $?)
  if [ $exists -eq 1 ]; then
    progExist $program
  elif [[ ! $manager = "apt-get install" ]] ; then
    echo "please download here"
    echo "https://syncthing.net/downloads/"
    return 2
  else
    sudo curl -o /usr/share/keyrings/syncthing-archive-keyring.gpg https://syncthing.net/release-key.gpg
    echo "deb [signed-by=/usr/share/keyrings/syncthing-archive-keyring.gpg] https://apt.syncthing.net/ syncthing stable" | sudo tee /etc/apt/sources.list.d/syncthing.list
    sudo apt-get update
    sudo apt-get install syncthing
  fi
}

function fzfInstall(){
  pm
  program="fzf"
  rec="git"
  exists=$(progExist $program > /dev/null 2>&1; echo $?)
  if [ $exists -eq 0 ]; then
    progRec $rec
    git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
    ~/.fzf/install
  else
    progExist $program
  fi 
  }

function gvim(){
  pm
  gui_exists=$(checkX  > /dev/null 2>&1; echo $?)
  if [ $gui_exists -eq 1 ];then
    checkX
    return 1
  fi
  program="gvim"
  prog_exists=$(progExist $program > /dev/null 2>&1; echo $?)
  if [ $prog_exists -eq 0 ]; then
    sudo $manager vim vim-gtk3
    echo -e "\" copy and past for the clipboard need vim-gtk3 to be installed for ubuntu. \nset clipboard=unnamedplus" >> file.txt
  else
    progExist $program
  fi 
  }


function repoInst(){
  program="$1" 
  exists=$(progExist $program > /dev/null 2>&1; echo $?)
  pm
  if [ $exists -eq 1 ] ; then
    progExist $program
  else
    sudo $manager $program
  fi
 }


function test(){
    echo -e "\" copy and past for the clipboard need vim-gtk3 to be installed for ubuntu. \nset clipboard=unnamedplus" >> file.txt

}

function pipInstAbr(){
  program="$1"
  abr="$2"
  exists=$(progExist $abr > /dev/null 2>&1; echo $?)
  if [ $exists -eq 1 ] ; then
    progExist $abr
  else
    echo "doesn't exist"
    return 1
    pip3 install  $program
  fi 
 }

function vundle(){
  pm
  directory="$HOME/.vim/bundle/Vundle.vim"
  program="vundle"
  rec="git"
  dir_exists=$(dirExists $directory > /dev/null 2>&1; echo $?)

  if [ $dir_exists -eq 0 ]; then
    progRec $rec
    git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
  else
    dirExists $directory $program
  fi
  }

function fisher(){
  pm
  file="$HOME/.config/fish/functions/fisher.fish"
  program="fisher"
  file_exists=$(dirExists $file > /dev/null 2>&1; echo $?)

  if [ $file_exists -eq 0 ]; then
    fzfInstall
    fish -c 'curl -sL https://git.io/fisher | source && fisher install jorgebucaran/fisher'
    fish -c 'fisher install PatrickF1/fzf.fish'
  else
    dirExists $file $program
  fi
  }

function detect(){
  echo -e "${bold}${yellow}Detected programs from the list${reset}"
  echo -e "${bold}${cyan}=================================${reset}"
  echo "2) $(progExist fish)"
  echo "3) $(progExist exa)"
  echo "4) $(progExist nala)"
  echo "5) $(progExist syncthing)"
  echo "6) $(progExist fzf)"
  echo "7) $(progExist vim)"
  echo "8) $(dirExists $HOME/.vim/bundle/Vundle.vim vundle)"
  echo "9) $(progExist yadm)"
  echo "10) $(dirExists $HOME/.config/fish/functions/fisher.fish fisher)"
  echo "11) $(progExist vf )"
  echo "12) $(progExist kitty)"
  echo "13) $(progExist gvim)"

  echo -e "${bold}${cyan}-------------------------------${reset}"
  echo
}



clear
echo
echo
echo -e "${bold}${yellow}Operating System${reset}"
echo -e "${bold}${cyan}================${reset}"
os
echo $0
echo $PWD
#dirname "$0"
#pt=dirname "$0"
#cp "$pt/test.txt ~"  
echo -e "${bold}${cyan}----------------${reset}"
echo
while true
do
echo
echo -e "${bold}${yellow}New Install:${green} V$version${reset}"
echo -e "${bold}${cyan}===================${reset}"
echo -e "${bold}${blue}1)${reset} check OS Version"
echo -e "${bold}${blue}2)${reset} Install ${green}fish shell${reset}: More user friendly shell then bash"
echo -e "${bold}${blue}3)${reset} Install ${green}Exa${reset}: Colorful ls"
echo -e "${bold}${blue}4)${reset} Install ${green}Nala${reset}: More Appealing apt"
echo -e "${bold}${blue}5)${reset} Install ${green}Syncthing${reset}: Sync files between computers"
echo -e "${bold}${blue}6)${reset} Install ${green}fzf${reset}: Fuzzy search"
echo -e "${bold}${blue}7)${reset} Install ${green}vim${reset}: Commandline text editor"
echo -e "${bold}${blue}8)${reset} Install ${green}Vundle${reset}: Vim plugin manager"
echo -e "${bold}${blue}9)${reset} Install ${green}yadm${reset}: git for dotfiles"
echo -e "${bold}${blue}10)${reset} Install ${green}fisher${reset}: fish plugin manager"
echo -e "${bold}${blue}11)${reset} Install ${green}virtualfish${reset}: Python virtual environments for fish shell"
echo
echo -e "${bold}${magenta}Graphical Environment Only${reset}"
echo -e "${bold}${red}-------------------${reset}"
echo -e "${bold}${blue}12)${reset} Install/Update${green} Kitty${reset}: Terminal emulator"
echo -e "${bold}${blue}13)${reset} Install ${green} vim gui${reset}: Vim clipboard support"
echo -e "${bold}${red}-------------------${reset}"

echo

echo -e "${bold}${blue}14)${reset} ${green}Detect installed${reset}"

echo -e "${bold}${red}Q)${reset}uit"
echo -e "${bold}${cyan}====================${reset}"
read -p "Enter your choice: " choice
echo
clear
case $choice in
  1) os;;
  2) fishShell;;
  3) exa;;
  4) nala;;
  5) syncthing;;
  6) fzfInstall;;
  7) repoInst vim;;
  8) vundle;;
  9) repoInst yadm;;
  10) fisher;;
  11) pipInstAbr virtualfish vf;;
  12) kitty;;
  13) gvim;;
  14) detect;;
  20) checkX;;
  21) progExist vim;;
  22) test;;
  q) exit 0;;
  *) echo -n "Unkown" ;;
esac
done
#eval $( echo -e "exa\nos\n"|fzf )


