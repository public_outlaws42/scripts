#!/bin/bash

echo "The shell or shell script: $0"
echo "Expands to the process ID of the job most recently placed into the background: $!"
echo "Process ID of the shell script:  $$"
echo "Exit code from last command: $?"
echo "All positional arguments:  $@"
echo "Expands to the current option flags as specified upon invocation:  $-"
